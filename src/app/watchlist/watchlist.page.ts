import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MovieService } from '../services/movie.service';
import { Result } from '../services/interfaces';
import { SkeletonLoaderComponent } from '../components/skeleton-loader/skeleton-loader.component';
import { MovieListComponent } from '../components/movie-list/movie-list.component';
import { ActivatedRoute, RouterLink } from '@angular/router';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.page.html',
  styleUrls: ['./watchlist.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule, SkeletonLoaderComponent, MovieListComponent, RouterLink]
})
export class WatchlistPage {

  private movieService = inject(MovieService);
  private route = inject(ActivatedRoute)

  movies: Result[] = [];
  isLoading: boolean = false;

  constructor() {
    this.route.params.subscribe((val)=> {
      this.getWatchlistMovies();
    })
  }
  
  getWatchlistMovies() {
    this.isLoading = true;
    this.movieService.getWatchList().subscribe({
      next: (res: any) => {
        this.movies = res.watchlist;
        this.isLoading = false;
      },
      error: (error) => {
        console.warn('error on getwatchlist :: ', error); 
        this.isLoading = false;
      }
    })
  }
  
}
