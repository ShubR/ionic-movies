import { Component, inject } from '@angular/core';
import {
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonList,
  IonItem,
  IonAvatar,
  IonSkeletonText,
  IonAlert,
  IonLabel,
  IonBadge,
  IonInfiniteScroll,
  IonIcon,
  IonButtons,
  IonButton,
  IonInput,
  IonSearchbar,
  SearchbarCustomEvent,
  SearchbarChangeEventDetail, IonInfiniteScrollContent
} from '@ionic/angular/standalone';
import { MovieService } from '../services/movie.service';
import { Result } from '../services/interfaces';
import {
  Subject,
  catchError,
  debounceTime,
  distinctUntilChanged,
  finalize,
  of,
} from 'rxjs';
import { DatePipe } from '@angular/common';
import { RouterLink } from '@angular/router';
import { MovieListComponent } from '../components/movie-list/movie-list.component';

@Component({
  selector: 'app-search',
  templateUrl: 'search.page.html',
  styleUrls: ['search.page.scss'],
  standalone: true,
  imports: [IonInfiniteScrollContent,
    IonSearchbar,
    IonInput,
    IonButton,
    IonButtons,
    IonIcon,
    IonInfiniteScroll,
    IonBadge,
    IonLabel,
    IonAlert,
    IonSkeletonText,
    IonAvatar,
    IonItem,
    IonList,
    IonHeader,
    IonToolbar,
    IonTitle,
    IonContent,
    IonList,
    DatePipe,
    RouterLink, MovieListComponent
  ],
})
export class SearchPage {
  private movieService = inject(MovieService);

  error = null;
  isLoading: boolean = false;
  public imageBaseUrl: string = 'https://image.tmdb.org/t/p';


  dummyArray: Array<any> = Array(5);

  movies: Result[] = [];

  searchSubject: Subject<string> = new Subject<string>();

  constructor() {
    this.searchSubject
      .pipe(debounceTime(1600), distinctUntilChanged())
      .subscribe((searchTerm: string) => {
        this.search(searchTerm);
      });
  }

  onSearchInput(event: any) {
    this.isLoading = true;
    this.error = null;
    this.searchSubject.next(event.target.value);
  }

  search(keyWord: string) {
    this.movieService.searchMovies(keyWord).pipe(finalize(() => {
      this.isLoading = false;
    })).subscribe({
      next: ((res: any) => {
        if (res.results) {
          this.movies = res.results;
        }
      }),
      error: ((err: any) => {
        console.warn('search failed :: ', err);

        if (err.error && err.error.status_message) {
          this.error = err.error.status_message;
        } else {
          this.error = err.statusText;
        }
      }),
    });
  }
}
