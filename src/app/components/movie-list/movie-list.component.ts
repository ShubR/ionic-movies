import { Component, Input, OnInit, inject } from '@angular/core';
import { Result } from 'src/app/services/interfaces';
import { RouterLink } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MovieService } from 'src/app/services/movie.service';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
  standalone: true,
  imports: [RouterLink, DatePipe],
})
export class MovieListComponent implements OnInit {
  @Input() movies: Result[] = [];
  @Input() isLoading: boolean = false;

  @Input() showDelete: boolean = false;

  movieService = inject(MovieService);
  alertService = inject(AlertService);

  public imageBaseUrl: string = 'https://image.tmdb.org/t/p';

  constructor() { }

  ngOnInit() { }

  deleteMovie(id: number) {

    this.alertService.showConfirmationDialog('Do you want to delete this movie?', 'Delete').subscribe({
      next: (deleteTriggered: boolean) => {
        if (deleteTriggered) {
          this.movieService.deleteMovie(id);
          this.movies = this.movies.filter((ele: any) => ele.id !== id);
        }
      }
    })

  }
}
