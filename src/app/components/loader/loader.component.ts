import { Component, OnInit, inject } from '@angular/core';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  standalone: true,
  imports: [NgxLoadingModule],
})
export class LoaderComponent implements OnInit {
  showLoader: boolean = false;

  loaderService = inject(LoaderService);

  loaderConfigs = {
    fullScreenBackdrop: true,
  };

  loaderText: string = 'Loading';

  constructor() {}

  ngOnInit() {
    this.loaderService.loadingTriggered().subscribe({
      next: (text: string) => {
        this.loaderText = text;
        this.showLoader = true;
      },
    });

    this.loaderService.dismissLoadingTrigger().subscribe({
      next: () => {
        this.showLoader = false;
      },
    });
  }
}
