import { Component, OnInit } from '@angular/core';
import { IonicModule } from '@ionic/angular';

@Component({
  selector: 'app-skeleton-loader',
  templateUrl: './skeleton-loader.component.html',
  styleUrls: ['./skeleton-loader.component.scss'],
  standalone: true,
  imports: [IonicModule]
})
export class SkeletonLoaderComponent  implements OnInit {

  dummyArray: Array<any> = Array(6);

  constructor() { }

  ngOnInit() {}

}
