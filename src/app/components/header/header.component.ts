import { Component, OnInit, inject } from '@angular/core';
import {
  Router,
  RouterLink,
} from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  standalone: true,
  imports: [RouterLink],
})
export class HeaderComponent implements OnInit {
  private authService = inject(AuthService);
  private router = inject(Router);
  private alertService = inject(AlertService)
  isLoggedin: boolean = false;
  currentRoute: string | undefined = '';

  constructor() { }

  ngOnInit() {
    this.router.events.subscribe({
      next: (event: any) => {
        this.currentRoute = event.url;
      },
    });

    this.isLoggedin = this.authService.validateLogin();

    this.authService.loginStatusChanged().subscribe({
      next: () => {
        this.isLoggedin = this.authService.validateLogin();
      },
    });
  }

  logout() {
    this.alertService.showConfirmationDialog('Do you want to logout?', 'Logout').subscribe({
      next: (triggerLogout: boolean) => {
        if (triggerLogout) {
          this.authService.logout();
        }
      }
    })

  }
}
