import {
  Component,
  Input,
  WritableSignal,
  inject,
  signal,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MovieService } from '../services/movie.service';
import { MovieResult, Result } from '../services/interfaces';
import { finalize } from 'rxjs';
import { LoaderService } from '../services/loader.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
  standalone: true,
  imports: [CommonModule, FormsModule],
})
export class DetailsPage {
  private movieService = inject(MovieService);

  private loaderService = inject(LoaderService);

  imageBaseUrl: string = 'https://image.tmdb.org/t/p';
  movie: WritableSignal<MovieResult | null> = signal(null);
  isLoading: boolean = false;

  moviePosterURL: string = '';

  @Input()
  set id(movieId: string) {
    this.isLoading = true;

    this.loaderService.showLoading('Fetching movie details');

    this.movieService
      .getMoviesDetails(movieId)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe({
        next: (res: any) => {
          this.movie.set(res);
          this.moviePosterURL = `${this.imageBaseUrl}/w500${res.backdrop_path}`;
          this.loaderService.dismissLoader();
        },
        error: (err) => {
          this.loaderService.dismissLoader();
        },
      });
  }

  constructor() {}

  addToWatchList() {
    const movie: any = this.movie();
    this.movieService.addToWatchList(movie);
  }
}
