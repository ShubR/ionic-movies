import { Component, inject } from '@angular/core';
import {
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  InfiniteScrollCustomEvent,
  IonList,
  IonItem,
  IonAvatar,
  IonSkeletonText,
  IonAlert,
  IonLabel,
  IonBadge,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonIcon,
  IonButtons,
  IonButton,
} from '@ionic/angular/standalone';
import { MovieService } from '../services/movie.service';
import { Result } from '../services/interfaces';
import { catchError, finalize, of } from 'rxjs';
import { DatePipe } from '@angular/common';
import { RouterLink } from '@angular/router';
import { MovieListComponent } from '../components/movie-list/movie-list.component';
import { SkeletonLoaderComponent } from '../components/skeleton-loader/skeleton-loader.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  standalone: true,
  imports: [
    IonButton,
    IonButtons,
    IonIcon,
    IonInfiniteScrollContent,
    IonInfiniteScroll,
    IonBadge,
    IonLabel,
    IonAlert,
    IonSkeletonText,
    IonAvatar,
    IonItem,
    IonList,
    IonHeader,
    IonToolbar,
    IonTitle,
    IonContent,
    IonList,
    DatePipe,
    RouterLink,
    MovieListComponent,
    SkeletonLoaderComponent
  ],
})
export class HomePage {
  private movieService = inject(MovieService);
  private currentPage: number = 1;
  error = null;
  isLoading: boolean = false;
  public imageBaseUrl: string = 'https://image.tmdb.org/t/p';

  dummyArray: Array<any> = Array(5);

  movies: Result[] = [];

  constructor() {
    this.loadMovies();
  }

  loadMovies(event?: InfiniteScrollCustomEvent) {
    this.error = null;
    this.isLoading = true;

    this.movieService
      .getTopRatedMovies(this.currentPage)
      .pipe(
        finalize(() => {
          this.isLoading = false;
          if (event) {
            event.target.complete();
          }
        }),
        catchError((err: any) => {
          console.warn(err);
          if (err.error && err.error.status_message) {
            this.error = err.error.status_message;
          } else {
            this.error = err.statusText;
          }
          return of(err);
        })
      )
      .subscribe({
        next: (res: any) => {
          if (res.results) {
            this.movies.push(...res.results);
            if (event) {
              event.target.disabled = res.total_pages === this.currentPage;
            }
          }
        },
      });
  }

  loadMore(event: InfiniteScrollCustomEvent) {
    this.currentPage++;
    this.loadMovies(event);
  }
}
