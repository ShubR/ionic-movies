import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { IonicModule, LoadingController } from '@ionic/angular';
import { AuthService } from '../auth.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule, ReactiveFormsModule, RouterLink]
})
export class LoginPage implements OnInit {

  loginForm: FormGroup = new FormGroup({});

  private authService = inject(AuthService);

  constructor(private fb: FormBuilder, private loader: LoadingController) { }

  ngOnInit() {

    if(this.authService.validateLogin()){
      this.authService.navigateToHome();
    }

    this.initLoginForm();
  }

  initLoginForm() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  login() {
    if(this.loginForm.invalid){
      this.loginForm.markAllAsTouched();
      return;
    }
    this.authService.authenticateUser(this.loginForm.value);    
  }


}
