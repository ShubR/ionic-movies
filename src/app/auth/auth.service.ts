import { Injectable, inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from './user-model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoaderService } from '../services/loader.service';
import { AlertService } from '../services/alert.service';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  loaderService = inject(LoaderService);
  alertService = inject(AlertService);

  private authSubject: Subject<void> = new Subject<void>();

  authenticateUser(loginCredentials: User) {
    this.loaderService.showLoading('Logging you in, please wait');

    this.http
      .post(`${environment.baseApiUrl}/user/login`, loginCredentials)
      .subscribe({
        next: (res: any) => {
          this.router.navigate(['watchlist']);
          localStorage.setItem('AuthToken', res.token);
          localStorage.setItem('expiryTime', res.expiryTime);

          this.loaderService.dismissLoader();
          this.alertService.showAlert('Login successful!');
          this.authSubject.next();
        },
        error: (error) => {
          console.warn('error on login :: ', error);
          this.loaderService.dismissLoader();
          this.alertService.showAlert('Login failed!');
        },
      });
  }

  registerUser(credentials: User) {
    this.loaderService.showLoading('Signing you up, please wait');

    this.http
      .post(`${environment.baseApiUrl}/user/signup`, credentials)
      .subscribe({
        next: (res: any) => {
          if (res.success) {
            this.alertService.showAlert(
              'Signup successful! Login to see your watchlist.'
            );
            this.router.navigate(['login']);
          }
          this.loaderService.dismissLoader();
        },
        error: (error) => {
          this.alertService.showAlert('Signup failed!');
          console.warn('error on register :: ', error);
          this.loaderService.dismissLoader();
        },
      });
  }

  validateLogin(): boolean {
    const expiryTime = localStorage.getItem('expiryTime');

    if (expiryTime) {
      return Date.now() < parseInt(expiryTime);
    }
    return false;
  }

  navigateToHome() {
    this.alertService.showAlert('You are already logged in!');
    this.router.navigate(['home']);
  }

  logout() {
    localStorage.clear();
    this.alertService.showAlert('Logged out!');
    this.router.navigate(['login']);
    this.authSubject.next();
  }

  loginStatusChanged(): Observable<void> {
    return this.authSubject.asObservable();
  }
}
