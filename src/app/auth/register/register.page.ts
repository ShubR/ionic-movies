import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
  standalone: true,
  imports: [ CommonModule, FormsModule, ReactiveFormsModule, RouterLink],
})
export class RegisterPage implements OnInit {
  signupForm: FormGroup = new FormGroup({});

  private authService = inject(AuthService);
  private router = inject(Router);

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    if (this.authService.validateLogin()) {
      this.authService.navigateToHome();
    }

    this.initSigupForm();
  }

  initSigupForm() {
    this.signupForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  signup() {
    if (this.signupForm.invalid) {
      this.signupForm.markAllAsTouched();
      return;
    }
    this.authService.registerUser(this.signupForm.value);
  }
}
