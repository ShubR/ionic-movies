import { Injectable, inject } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  private alertController = inject(AlertController);

  alert: any;

  constructor() { }

  async showAlert(text: string, buttonText: string = 'Ok') {
    this.alert = await this.alertController.create({
      header: text,
      buttons: [buttonText],
    });

    this.alert.present();

  }

  showConfirmationDialog(message: string, buttonText: string): Observable<boolean> {

    return new Observable(observer => {
      this.alertController.create({
        header: 'Confirmation',
        message: message,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'cancel-button-class',
            handler: () => {
              observer.next(false);
              observer.complete();
            }
          },
          {
            text: buttonText,
            cssClass: 'logout-button-class',
            handler: () => {
              observer.next(true);
              observer.complete();
            }
          }
        ]
      }).then(alert => {
        alert.present();
      });
    });
  }

}
