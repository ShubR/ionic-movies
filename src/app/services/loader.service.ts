import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  loader: any;

  private loaderSubject: Subject<string> = new Subject<string>();

  private dismissLoaderSubject: Subject<void> = new Subject<void>();

  constructor() {}

  showLoading(text: string) {
    this.loaderSubject.next(text);
  }

  dismissLoader() {
    this.dismissLoaderSubject.next();
  }

  loadingTriggered(): Observable<string> {
    return this.loaderSubject.asObservable();
  }

  dismissLoadingTrigger(): Observable<void> {
    return this.dismissLoaderSubject.asObservable();
  }
}
