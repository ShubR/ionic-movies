import { Injectable } from '@angular/core';
import { MovieResult } from './interfaces';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  
  watchList: Array<MovieResult> = [];
  
  watchListSubject: BehaviorSubject<MovieResult[]> = new BehaviorSubject<MovieResult[]>([]);
  
  constructor() { 
    
    // this.watchList = JSON.parse(localStorage.getItem('watchList'))
    // this.watchList = 
  }

  setWatchList(movie: MovieResult) {
    this.watchList.push(movie);
    // localStorage.setItem('watchList', this.)
  }


}
