import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Result } from './interfaces';
import { LoaderService } from './loader.service';
import { AlertService } from './alert.service';

const BASE_URL = environment.baseUrl;

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  private http = inject(HttpClient);
  private loaderService = inject(LoaderService);
  private alertService = inject(AlertService);

  constructor() {}

  getTopRatedMovies(page: number = 1) {
    return this.http.get(
      `${BASE_URL}/movie/popular?api_key=${environment.apiKey}&page=${page}`
    );
  }

  getMoviesDetails(id: string) {
    return this.http.get(
      `${BASE_URL}/movie/${id}?api_key=${environment.apiKey}`
    );
  }

  searchMovies(keyWord: string) {
    return this.http.get(
      `${BASE_URL}/search/movie?api_key=${environment.apiKey}&language=en-US&page=1&query=${keyWord}`
    );
  }

  getWatchList() {
    const token = localStorage.getItem('AuthToken');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);

    return this.http.get(`${environment.baseApiUrl}/movies/getWatchlist`, {
      headers,
    });
  }

  addToWatchList(movie: Result) {
    const token = localStorage.getItem('AuthToken');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);

    this.loaderService.showLoading('Adding movie to watchlist');

    this.http
      .post(`${environment.baseApiUrl}/movies/addToWatchlist`, movie, {
        headers,
      })
      .subscribe({
        next: (res: any) => {
          console.info(res);
          this.loaderService.dismissLoader();
          this.alertService.showAlert(res.message);
        },
        error: (err) => {
          this.alertService.showAlert(err.error.message);
          this.loaderService.dismissLoader();
        },
      });
  }

  deleteMovie(id: number) {
    const token = localStorage.getItem('AuthToken');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);

    this.loaderService.showLoading('Deleting movie from your watchlist...');

    this.http
      .delete(`${environment.baseApiUrl}/movies/${id}`, { headers })
      .subscribe({
        next: (res: any) => {
          this.alertService.showAlert(res.message);
          this.loaderService.dismissLoader();
        },
        error: (err: any) => {
          this.alertService.showAlert(err.error.message);
          this.loaderService.dismissLoader();
        },
      });
  }
}
